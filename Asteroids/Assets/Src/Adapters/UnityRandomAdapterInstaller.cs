﻿using Zenject;
using UnityEngine;
using GameCore.Gameplay.Interfaces;

namespace Adapters
{
    class UnityRandomAdapterInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<IRandom>().To<UnityRandomAdapter>().AsTransient();
        }
    }
}

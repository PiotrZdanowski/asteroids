﻿using System;
using UI.MainMenu;

public class MainMenuUIAdapter : GameCore.MainMenu.Interfaces.IMainMenuUI
{
    IMainMenuUI mainMenuUI;

    public MainMenuUIAdapter(IMainMenuUI mainMenuUI)
    {
        this.mainMenuUI = mainMenuUI ?? throw new ArgumentNullException(nameof(mainMenuUI));
    }

    public void SetStartButtonAction(Action action)
    {
        mainMenuUI.SetStartButtonAction(action);
    }

    public void ShowHighScore(int score)
    {
        mainMenuUI.ShowHighScore(score);
    }
}

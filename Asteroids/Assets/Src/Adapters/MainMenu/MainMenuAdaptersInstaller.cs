using Adapters.SaveSystem;
using UI.MainMenu;
using UnityEngine;
using Utils.ScoreSystem.Interfaces;
using Zenject;

public class MainMenuAdaptersInstaller : MonoInstaller
{
    [SerializeField] private MainMenuUI mainMenuUI = null;
    [SerializeField] private ScriptableScoreInMemorySaveSystemAdapter saveSystem = null;

    public override void InstallBindings()
    {
        Container.Bind<IMainMenuUI>().FromInstance(mainMenuUI).AsSingle();
        Container.Bind<GameCore.MainMenu.Interfaces.IMainMenuUI>().To<MainMenuUIAdapter>().FromNew().AsTransient();
        Container.Bind<ISaveSystem>().FromInstance(saveSystem).AsSingle();
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Utils.SaveSystem.Concrete;
using Zenject;

namespace Adapters.SaveSystem
{
    [CreateAssetMenu(fileName = "InMemorySave", menuName = "Asteroids/SaveSystem/InMemorySave")]
    public class ScriptableScoreInMemorySaveSystemAdapter : ScriptableObject, Utils.ScoreSystem.Interfaces.ISaveSystem
    {
        [SerializeField] private string highscoreSaveKey = "highscore";
        [SerializeField] private InMemorySave saveSystem = new InMemorySave();

        public int GetHighscore()
        {
            if (saveSystem.Contains<int>(highscoreSaveKey))
            {
                return saveSystem.Get<int>(highscoreSaveKey);
            }
            else
            {
                return 0;
            }
        }

        public void SaveHighscore(int score)
        {
            saveSystem.Save<int>(highscoreSaveKey, score);
        }
    }
}

﻿using GameCore.Gameplay.Interfaces;
using UnityEngine;

namespace Adapters
{
    class UnityRandomAdapter : IRandom
    {
        public float Value => Random.value;

        public float Range(float v1, float v2)
        {
            return Random.Range(v1, v2);
        }

        public float Range(float v)
        {
            return Range(0, v);
        }

        public int Range(int v)
        {
            return Random.Range(0, v);
        }
    }
}

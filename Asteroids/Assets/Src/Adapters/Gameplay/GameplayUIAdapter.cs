﻿using GameCore.Gameplay.Interfaces;
using GameCore.Gameplay.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UI.Gameplay.Interfaces;
using UnityEngine;
using Zenject;

namespace Adapters.Gameplay
{
    public class GameplayUIAdapter : MonoBehaviour, IGameplayUI
    {
        [Inject] private IPlayerUI playerUI;
        [Inject] private IScoreUI scoreUI;
        [Inject] private IGameOverUI gameOverUI;

        public void ShowEndButtons(EndButtonsData endButtonsData)
        {
            gameOverUI.ShowEndButtons(new UI.Gameplay.ValueObjects.EndButtonsData(endButtonsData.score, endButtonsData.OnMainMenuPressed, endButtonsData.OnRestartButtonPressed));
        }

        public void ShowHighscoreBonus()
        {
            scoreUI.ShowHighscoreBonus();
        }

        public void UpdatePlayerView(PlayerViewData playerViewData)
        {
            playerUI.UpdatePlayerView(new UI.Gameplay.ValueObjects.PlayerViewData(playerViewData.hearts, playerViewData.maxHearts));
        }

        public void UpdateScoreView(ScoreViewData scoreViewData)
        {
            scoreUI.UpdateScoreView(new UI.Gameplay.ValueObjects.ScoreViewData(scoreViewData.highscore, scoreViewData.score));
        }
    }
}

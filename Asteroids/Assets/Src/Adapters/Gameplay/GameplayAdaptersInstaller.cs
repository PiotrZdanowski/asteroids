using Adapters.Gameplay;
using Adapters.SaveSystem;
using GameCore.Gameplay.Interfaces;
using UI.Gameplay.Concrete;
using UI.Gameplay.Interfaces;
using UnityEngine;
using Utils.ScoreSystem.Interfaces;
using Zenject;

public class GameplayAdaptersInstaller : MonoInstaller
{
    [SerializeField] private ScriptableScoreInMemorySaveSystemAdapter saveSystem = null;
    [SerializeField] private GameplayUIAdapter UI = null;
    [SerializeField] private PlayerUI playerUI = null;
    [SerializeField] private ScoreUI scoreUI = null;
    [SerializeField] private GameOverUI gameOverUI = null;


    public override void InstallBindings()
    {
        Container.Bind<ISaveSystem>().FromInstance(saveSystem).AsSingle();
        Container.Bind<IGameplayUI>().FromInstance(UI).AsSingle();
        Container.Bind<IPlayerUI>().FromInstance(playerUI).AsSingle();
        Container.Bind<IScoreUI>().FromInstance(scoreUI).AsSingle();
        Container.Bind<IGameOverUI>().FromInstance(gameOverUI).AsSingle();
    }
}
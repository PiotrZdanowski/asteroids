﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI.MainMenu
{
    public interface IMainMenuUI
    {
        void SetStartButtonAction(Action action);
        void ShowHighScore(int score);
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI.MainMenu
{
    public class MainMenuUI : MonoBehaviour, IMainMenuUI
    {
        [SerializeField] private Text highscoreText;
        [SerializeField] private string highscoreShowingFormat;
        private Action startButtonAction;

        public void SetStartButtonAction(Action action)
        {
            startButtonAction = action;
        }

        public void OnStartButtonClick()
        {
            startButtonAction.Invoke();
        }

        public void ShowHighScore(int score)
        {
            highscoreText.text = string.Format(highscoreShowingFormat, score);
        }
    }
}
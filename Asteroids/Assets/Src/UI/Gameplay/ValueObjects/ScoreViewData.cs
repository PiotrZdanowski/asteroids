﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI.Gameplay.ValueObjects
{
    public readonly struct ScoreViewData
    {
        public readonly int highscore;
        public readonly int score;

        public ScoreViewData(int highscore, int score)
        {
            this.highscore = highscore;
            this.score = score;
        }
    }
}

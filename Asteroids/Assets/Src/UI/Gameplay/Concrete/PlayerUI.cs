﻿using UI.Gameplay.Interfaces;
using UI.Gameplay.ValueObjects;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Gameplay.Concrete
{
    public class PlayerUI : MonoBehaviour, IPlayerUI
    {
        [SerializeField] private Image[] playerHearts;
        [SerializeField] private float disabledAlpha;

        public void UpdatePlayerView(PlayerViewData playerViewData)
        {
            if(playerHearts.Length != playerViewData.maxHearts)
            {
                throw new System.Exception(string.Format("Expected {0} hearts, found {1}!", playerViewData.maxHearts, playerHearts.Length));
            }
            int wholeHeartsLeft = playerViewData.hearts;
            for (int i = playerHearts.Length; i-- > wholeHeartsLeft;)
            {
                playerHearts[i].color = new Color(playerHearts[i].color.r, playerHearts[i].color.g, playerHearts[i].color.b, disabledAlpha);
            }
        }
    }
}

﻿using UI.Gameplay.Interfaces;
using UI.Gameplay.ValueObjects;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Gameplay.Concrete
{
    public class ScoreUI : MonoBehaviour, IScoreUI
    {
        [SerializeField] private Text currentScoreText = null;
        [SerializeField] private string curentScoreFormat = "{0}";
        [SerializeField] private Text highscoreText = null;
        [SerializeField] private string highscoreFormat = "{0}";
        [SerializeField] private GameObject highscoreNotification = null;

        private void Awake()
        {
            highscoreNotification.SetActive(false);
        }

        public void ShowHighscoreBonus()
        {
            highscoreNotification.SetActive(true);
        }

        public void UpdateScoreView(ScoreViewData scoreViewData)
        {
            currentScoreText.text = string.Format(curentScoreFormat, scoreViewData.score);
            highscoreText.text = string.Format(highscoreFormat, scoreViewData.highscore);
        }
    }
}

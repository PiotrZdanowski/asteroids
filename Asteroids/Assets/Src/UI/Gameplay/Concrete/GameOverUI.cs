﻿using System;
using UI.Gameplay.Interfaces;
using UI.Gameplay.ValueObjects;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Gameplay.Concrete
{
    public class GameOverUI : MonoBehaviour, IGameOverUI
    {
        [SerializeField] private Text scoreText = null;
        [SerializeField] private string scoreFormat = "{0}";
        [SerializeField] private GameObject parent = null;

        private Action onMainMenuButtonPressed;
        private Action onRestartButtonPressed;

        private void Awake()
        {
            parent.SetActive(false);
        }

        public void ShowEndButtons(EndButtonsData endButtonsData)
        {
            scoreText.text = string.Format(scoreFormat, endButtonsData.score);
            onMainMenuButtonPressed = endButtonsData.OnMainMenuPressed;
            onRestartButtonPressed = endButtonsData.OnRestartButtonPressed;
            parent.SetActive(true);
        }

        public void OnMainMenuButtonPressed()
        {
            onMainMenuButtonPressed?.Invoke();
        }

        public void OnRestartButtonPressed()
        {
            onRestartButtonPressed?.Invoke();
        }
    }
}

﻿using UI.Gameplay.ValueObjects;

namespace UI.Gameplay.Interfaces
{
    public interface IPlayerUI
    {
        void UpdatePlayerView(PlayerViewData playerViewData);
    }
}

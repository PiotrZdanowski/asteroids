﻿using UI.Gameplay.ValueObjects;

namespace UI.Gameplay.Interfaces
{
    public interface IGameOverUI
    {
        void ShowEndButtons(EndButtonsData endButtonsData);
    }
}

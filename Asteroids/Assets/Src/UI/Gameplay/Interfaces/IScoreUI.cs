﻿using UI.Gameplay.ValueObjects;

namespace UI.Gameplay.Interfaces
{
    public interface IScoreUI
    {
        void UpdateScoreView(ScoreViewData scoreViewData);

        void ShowHighscoreBonus();
    }
}

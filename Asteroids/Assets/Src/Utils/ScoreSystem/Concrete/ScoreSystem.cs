﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils.ScoreSystem.Interfaces;

namespace Utils.ScoreSystem.Concrete
{
    public class ScoreSystem : IScoreSystem
    {
        private ISaveSystem saveSystem;
        private int? highscore = null;
        private bool highscoreReached = false;

        public int CurrentScore { get; private set; } = 0;
        public int Highscore { get 
            {
                if (!highscore.HasValue)
                {
                    highscore = saveSystem.GetHighscore();
                }
                return highscore.Value;
            } 
        }

        public event Action<IScoreSystem, int> OnScoreChanged = delegate { };
        public event Action<IScoreSystem, int> OnHighscoreReached = delegate { };

        public ScoreSystem(ISaveSystem saveSystem)
        {
            this.saveSystem = saveSystem ?? throw new ArgumentNullException(nameof(saveSystem));
        }

        public void AddScore(int score)
        {
            CurrentScore += score;
            if(CurrentScore > Highscore)
            {
                highscore = CurrentScore;
                saveSystem.SaveHighscore(highscore.Value);
                if (!highscoreReached)
                {
                    highscoreReached = true;
                    OnHighscoreReached(this, CurrentScore);
                }
            }
            OnScoreChanged(this, CurrentScore);
        }
    }
}

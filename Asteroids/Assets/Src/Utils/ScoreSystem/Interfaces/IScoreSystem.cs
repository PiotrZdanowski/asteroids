﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils.ScoreSystem.Interfaces
{
    public interface IScoreSystem
    {
        int Highscore { get; }
        int CurrentScore { get; }
        void AddScore(int score);

        event Action<IScoreSystem, int> OnScoreChanged;

        event Action<IScoreSystem, int> OnHighscoreReached;
    }
}

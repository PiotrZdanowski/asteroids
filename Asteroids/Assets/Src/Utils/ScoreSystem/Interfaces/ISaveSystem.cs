﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils.ScoreSystem.Interfaces
{
    public interface ISaveSystem
    {
        void SaveHighscore(int score);

        int GetHighscore();
    }
}

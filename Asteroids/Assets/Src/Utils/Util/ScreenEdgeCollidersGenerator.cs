﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Util
{
    public class ScreenEdgeCollidersGenerator
    {
        private Camera camera;

        public ScreenEdgeCollidersGenerator(Camera camera)
        {
            this.camera = camera ?? throw new ArgumentNullException(nameof(camera));
        }

        public void Generate2DColliders(EdgeCollider2D edgeCollider, float distance, float offset)
        {
            Vector3[] corners = new Vector3[4];
            camera.CalculateFrustumCorners(new Rect(0, 0, 1, 1), distance, Camera.MonoOrStereoscopicEye.Mono, corners);
            Vector2[] points = new Vector2[5];
            for(int i = 0; i< corners.Length; i++)
            {
                Vector2 point = camera.transform.TransformVector(corners[i]);
                point += point.normalized * offset;
                points[i] = point;
            }
            Vector2 lastpoint = camera.transform.TransformVector(corners[0]);
            lastpoint += lastpoint.normalized * offset;
            points[points.Length - 1] = lastpoint;
            edgeCollider.points = points;
        }
    }
}

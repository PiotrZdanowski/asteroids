﻿using UnityEngine;
using Utils.Characters.Movement.Concrete;
using Utils.Characters.Movement.Interfaces;
using Utils.Characters.Movement.ValueObjects;
using Utils.Characters.UnityAdapters;

namespace Utils.Characters.UnityAdapters
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class SpaceshipMovement2DScript : MonoBehaviour, ISpaceshipMovement2D
    {
        [SerializeField] private float movementMaxSpeed = 1;
        [SerializeField] private float movementForce = 1;
        [SerializeField] private float rotationSpeed = 180;

        private ISpaceshipMovement2D spaceshipMovement2D;
        private ForwardRigidbody2DMovement forward;

        public float RotationSpeed { get => spaceshipMovement2D.RotationSpeed; set { spaceshipMovement2D.RotationSpeed = value; rotationSpeed = value; } }
        public float MaxSpeed { get => spaceshipMovement2D.MaxSpeed; set { spaceshipMovement2D.MaxSpeed = value; movementMaxSpeed = value; } }
        public float MovementForce { get => forward.MovementForce; set { forward.MovementForce = value; movementForce = value; } }

        private void Awake()
        {
            Rigidbody2D rigidbody = gameObject.GetComponent<Rigidbody2D>();
            TransformAdapter transformAdapter = new TransformAdapter(transform, rigidbody);
            forward = new ForwardRigidbody2DMovement(transformAdapter, new Rigidbody2DAdapter(transform, rigidbody), movementForce, movementMaxSpeed);
            spaceshipMovement2D = new SpaceshipMovement2D(new Rotation2D(transformAdapter, new TimeAdapter(), rotationSpeed), forward);
        }

        public void MoveForward()
        {
            spaceshipMovement2D.MoveForward();
        }

        public void Rotate(Direction direction)
        {
            spaceshipMovement2D.Rotate(direction);
        }

        private void OnValidate()
        {
            if(forward != null)
            {
                forward.MovementForce = movementForce;
            }
            if(spaceshipMovement2D != null)
            {
                spaceshipMovement2D.MaxSpeed = movementMaxSpeed;
                spaceshipMovement2D.RotationSpeed = rotationSpeed;
            }
        }

        public void Stop()
        {
            spaceshipMovement2D.Stop();
        }
    }
}

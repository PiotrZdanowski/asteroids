﻿using UnityEngine;
using Utils.Characters.Damage.Interfaces;

namespace Utils.CharactersUnityAdapters.Components
{
    public class DamageEveryIDamageableOnTrigger : MonoBehaviour, IDamageSource
    {
        [SerializeField] private float damageValue = float.PositiveInfinity;

        private void OnTriggerEnter(Collider other)
        {
            DamageIfNotNull(other.GetComponent<IDamageable>());
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            DamageIfNotNull(collision.GetComponent<IDamageable>());
        }

        protected void DamageIfNotNull(IDamageable damageable)
        {
            if (damageable != null)
            {
                damageable.Damage(damageValue, this);
            }
        }
    }
}

﻿using System;
using UnityEngine;
using UnityEngine.Events;
using Utils.Characters.Damage.Interfaces;

namespace Utils.CharactersUnityAdapters.Components
{
    public class DamagingCollisionsDetector : MonoBehaviour
    {
        public event Action<IDamageable> OnDamageableHit = delegate { };

        private void OnCollisionEnter(Collision collision)
        {
            SendSignalIfNotNull(collision.gameObject.GetComponent<IDamageable>());
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            SendSignalIfNotNull(collision.gameObject.GetComponent<IDamageable>());
        }

        private void SendSignalIfNotNull(IDamageable damageable)
        {
            if(damageable != null)
            {
                OnDamageableHit(damageable);
            }
        }
    }
}

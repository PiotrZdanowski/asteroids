﻿using System;
using UnityEngine;
using Utils.Characters.Damage.Interfaces;

namespace Utils.CharactersUnityAdapters.Components
{
    public class DamagingTriggerDetector : MonoBehaviour
    {
        public event Action<IDamageable> OnDamageableHit = delegate { };

        private void OnTriggerEnter(Collider collision)
        {
            SendSignalIfNotNull(collision.gameObject.GetComponent<IDamageable>());
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
             SendSignalIfNotNull(collision.gameObject.GetComponent<IDamageable>());
        }

        private void SendSignalIfNotNull(IDamageable damageable)
        {
            if (damageable != null)
            {
                OnDamageableHit(damageable);
            }
        }
    }
}

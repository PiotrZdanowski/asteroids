﻿using System;
using UnityEngine;
using Utils.Characters.Movement.Interfaces;

namespace Utils.Characters.UnityAdapters
{
    public class TransformAdapter : ITransform
    {
        private Transform transform;
        private Rigidbody rigidbody;
        private Rigidbody2D rigidbody2D;

        public Vector3 Up => transform.up;

        public TransformAdapter(Transform transform)
        {
            this.transform = transform ?? throw new ArgumentNullException(nameof(transform));
        }

        public TransformAdapter(Transform transform, Rigidbody rigidbody)
        {
            this.transform = transform ?? throw new ArgumentNullException(nameof(transform));
            this.rigidbody = rigidbody ?? throw new ArgumentNullException(nameof(rigidbody));
        }

        public TransformAdapter(Transform transform, Rigidbody2D rigidbody)
        {
            this.transform = transform ?? throw new ArgumentNullException(nameof(transform));
            this.rigidbody2D = rigidbody ?? throw new ArgumentNullException(nameof(rigidbody));
        }

        public void Rotate(float x, float y, float z)
        {
            if(rigidbody != null)
            {
                rigidbody.rotation = (Quaternion.Euler(rigidbody.rotation.eulerAngles + new Vector3(x, y, z)));
            }
            else if(rigidbody2D != null)
            {
                rigidbody2D.rotation = rigidbody2D.rotation + z;
            }
            else
            {
                transform.Rotate(x, y, z);
            }
        }
    }
}

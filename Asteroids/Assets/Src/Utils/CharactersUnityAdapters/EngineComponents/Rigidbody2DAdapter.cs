﻿using System;
using UnityEngine;
using Utils.Characters.Movement.Interfaces;

namespace Utils.Characters.UnityAdapters
{
    class Rigidbody2DAdapter : IRigidbody2D
    {
        private Transform transform;
        private Rigidbody2D rigidbody;

        public Rigidbody2DAdapter(Transform transform, Rigidbody2D rigidbody)
        {
            this.transform = transform ?? throw new ArgumentNullException(nameof(transform));
            this.rigidbody = rigidbody ?? throw new ArgumentNullException(nameof(rigidbody));
        }

        public Vector2 RelativeVelocity => transform.InverseTransformDirection(rigidbody.velocity);

        public void AddWorldForce(Vector2 force)
        {
            rigidbody.AddForce(force);
        }

        public void Stop()
        {
            rigidbody.velocity = Vector2.zero;
            rigidbody.angularVelocity = 0;
        }
    }
}

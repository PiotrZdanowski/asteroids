﻿using UnityEngine;
using Utils.Characters.Movement.Interfaces;

namespace Utils.Characters.UnityAdapters
{
    public class TimeAdapter : ITime
    {
        public float DeltaTime => Time.deltaTime;
    }
}

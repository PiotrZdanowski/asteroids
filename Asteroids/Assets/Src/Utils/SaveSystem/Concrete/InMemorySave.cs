﻿using System;
using System.Collections.Generic;
using Utils.SaveSystem.Interface;

namespace Utils.SaveSystem.Concrete
{
    public class InMemorySave : ISaveSystem
    {
        private Dictionary<string, object> save = new Dictionary<string, object>();

        public bool Contains<T>(string key)
        {
            return save.ContainsKey(key) && save[key] is T;
        }

        public T Get<T>(string key)
        {
            if (Contains<T>(key))
            {
                return (T)save[key];
            }
            else
            {
                throw new Exception(string.Format("Key '{0}' not found!", key));
            }
        }

        public void Save<T>(string key, T value)
        {
            if (save.ContainsKey(key))
            {
                save[key] = value;
            }
            else
            {
                save.Add(key, value);
            }
        }
    }
}

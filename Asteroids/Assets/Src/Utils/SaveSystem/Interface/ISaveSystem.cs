﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils.SaveSystem.Interface
{
    public interface ISaveSystem
    {
        bool Contains<T>(string key);

        void Save<T>(string key, T value);

        T Get<T>(string key);
    }
}

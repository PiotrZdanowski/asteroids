﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils.StateMachine
{
    public interface IStateMachine<TStartState, TFactoryState> where TStartState : IState where TFactoryState : IStatesFactory
    {
        IState CurrentState { get; }

        void Initialize();

        void SwitchToState<TState>() where TState : IState;

        void Update();

        void FixedUpdate();
    }

    public interface IStateMachine<TStartState> : IStateMachine<TStartState, IStatesFactory> where TStartState : IState
    {
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils.StateMachine
{
    public abstract class BaseState : IState
    {
        public virtual void Deinitialize()
        {

        }

        public virtual void FixedUpdateState()
        {

        }

        public virtual void Initialize()
        {

        }

        public virtual void UpdateState()
        {

        }
    }
}

﻿using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("StateMachineTests")]

namespace Utils.StateMachine
{
    public class StateMachine<TStartState, TStatesFactory> : IStateMachine<TStartState, TStatesFactory> where TStartState : IState where TStatesFactory : class, IStatesFactory
    {
        private TStatesFactory statesFactory;

        public IState CurrentState { get; private set; } = null;

        public StateMachine(TStatesFactory statesFactory)
        {
            this.statesFactory = statesFactory ?? throw new ArgumentNullException(nameof(statesFactory));
        }

        public void Initialize()
        {
            SwitchToState<TStartState>();
        }

        public void SwitchToState<TState>() where TState : IState
        {
            CurrentState?.Deinitialize();
            CurrentState = statesFactory.CteateState<TState>(CurrentState);
            CurrentState.Initialize();
        }
        public void FixedUpdate()
        {
            CurrentState.FixedUpdateState();
        }

        public void Update()
        {
            CurrentState.UpdateState();
        }
    }

    public class StateMachine<TStartState> : StateMachine<TStartState, IStatesFactory>, IStateMachine<TStartState> where TStartState : IState
    {
        public StateMachine(IStatesFactory statesFactory) : base(statesFactory) { }
    }
}

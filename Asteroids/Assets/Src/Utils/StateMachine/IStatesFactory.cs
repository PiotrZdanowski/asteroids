﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils.StateMachine
{
    public interface IStatesFactory
    {
        TState CteateState<TState>(IState currentState = null) where TState : IState;
    }
}

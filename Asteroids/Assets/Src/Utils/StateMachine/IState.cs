﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils.StateMachine
{
    public interface IState
    {
        void Initialize();

        void Deinitialize();

        void UpdateState();

        void FixedUpdateState();
    }
}

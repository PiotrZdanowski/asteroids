﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils.ObjectPooling.Interfaces
{
    public interface IObjectPoolObject
    {
        void Reset();
    }
}

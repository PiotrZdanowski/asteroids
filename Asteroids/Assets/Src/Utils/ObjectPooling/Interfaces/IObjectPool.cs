﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utils.ObjectPooling.Interfaces
{
    public interface IObjectPool<TObject> where TObject : IObjectPoolObject
    {
        TObject GetNext();

        void ReturnToPool(TObject obj);
    }
}

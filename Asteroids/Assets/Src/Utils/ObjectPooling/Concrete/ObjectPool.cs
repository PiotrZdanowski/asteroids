﻿using System;
using System.Collections.Generic;
using Utils.ObjectPooling.Interfaces;

namespace Utils.ObjectPooling.Concrete
{
    public class ObjectPool<TObject> : IObjectPool<TObject> where TObject : IObjectPoolObject
    {
        private IObjectFactory<TObject> objectFactory;
        private Queue<TObject> objectsPool;

        public ObjectPool(IObjectFactory<TObject> objectFactory)
        {
            this.objectFactory = objectFactory ?? throw new ArgumentNullException(nameof(objectFactory));
            objectsPool = new Queue<TObject>();
        }

        public ObjectPool(IObjectFactory<TObject> objectFactory, Queue<TObject> objectsPool)
        {
            this.objectFactory = objectFactory ?? throw new ArgumentNullException(nameof(objectFactory));
            this.objectsPool = objectsPool ?? throw new ArgumentNullException(nameof(objectsPool));
        }

        public TObject GetNext()
        {
            if(objectsPool.Count > 0)
            {
                TObject obj = objectsPool.Dequeue();
                obj.Reset();
                return obj;
            }
            else
            {
                return objectFactory.CreateNewObject();
            }
        }

        public void ReturnToPool(TObject obj)
        {
            obj.Reset();
            objectsPool.Enqueue(obj);
        }
    }
}

﻿using Utils.Characters.Movement.ValueObjects;

namespace Utils.Characters.Movement.Interfaces
{
    public interface IRotation2D
    {
        float RotationSpeed { get; set; }

        void Rotate(Direction direction);
    }
}

﻿using UnityEngine;

namespace Utils.Characters.Movement.Interfaces
{
    public interface IRigidbody2D
    {
        Vector2 RelativeVelocity { get; }

        void AddWorldForce(Vector2 force);
        void Stop();
    }
}

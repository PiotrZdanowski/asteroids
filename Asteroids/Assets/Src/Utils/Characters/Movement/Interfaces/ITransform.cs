﻿using UnityEngine;

namespace Utils.Characters.Movement.Interfaces
{
    public interface ITransform
    {
        Vector3 Up { get; }
        void Rotate(float x, float y, float z);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils.Characters.Movement.Interfaces
{
    public interface ITime
    {
        float DeltaTime { get; }
    }
}

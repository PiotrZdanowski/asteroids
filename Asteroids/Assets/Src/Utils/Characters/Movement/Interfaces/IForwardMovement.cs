﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utils.Characters.Movement.Interfaces
{
    public interface IForwardMovement
    {
        float MaxSpeed { get; set; }

        void MoveForward();
        void Stop();
    }
}

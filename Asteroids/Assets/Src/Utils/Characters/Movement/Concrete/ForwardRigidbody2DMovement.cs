﻿using System;
using UnityEngine;
using Utils.Characters.Movement.Interfaces;

namespace Utils.Characters.Movement.Concrete
{
    public class ForwardRigidbody2DMovement : IForwardMovement
    {
        private ITransform transform;
        private IRigidbody2D rigidbody;

        public float MovementForce { get; set; }
        public float MaxSpeed { get; set; }

        public ForwardRigidbody2DMovement(ITransform transform, IRigidbody2D rigidbody, float movementForce = 1, float maxSpeed = 1)
        {
            this.transform = transform ?? throw new ArgumentNullException(nameof(transform));
            this.rigidbody = rigidbody ?? throw new ArgumentNullException(nameof(rigidbody));
            this.MovementForce = movementForce;
            this.MaxSpeed = maxSpeed;
        }

        public void MoveForward()
        {
            Vector2 relativeVelocity = rigidbody.RelativeVelocity;
            if (relativeVelocity.y < MaxSpeed)
            {
                rigidbody.AddWorldForce(transform.Up * MovementForce);
            }
        }

        public void Stop()
        {
            rigidbody.Stop();
        }
    }
}

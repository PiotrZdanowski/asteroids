﻿using System;
using Utils.Characters.Movement.Interfaces;
using Utils.Characters.Movement.ValueObjects;

namespace Utils.Characters.Movement.Concrete
{
    public class Rotation2D : IRotation2D
    {
        private ITransform transform;
        private ITime time;

        public float RotationSpeed { get; set; }

        public Rotation2D(ITransform transform, ITime time, float rotationSpeed)
        {
            this.transform = transform ?? throw new ArgumentNullException(nameof(transform));
            this.time = time ?? throw new ArgumentNullException(nameof(time));
            RotationSpeed = rotationSpeed;
        }

        public void Rotate(Direction direction)
        {
            transform.Rotate(0, 0, direction * RotationSpeed * time.DeltaTime);
        }
    }
}

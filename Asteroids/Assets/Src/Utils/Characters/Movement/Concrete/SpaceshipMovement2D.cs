﻿using System;
using Utils.Characters.Movement.Interfaces;
using Utils.Characters.Movement.ValueObjects;

namespace Utils.Characters.Movement.Concrete
{
    public class SpaceshipMovement2D : ISpaceshipMovement2D
    {
        private IRotation2D rotation;
        private IForwardMovement forwardMovement;

        public float RotationSpeed { get => rotation.RotationSpeed; set => rotation.RotationSpeed = value; }
        public float MaxSpeed { get => forwardMovement.MaxSpeed; set => forwardMovement.MaxSpeed = value; }

        public SpaceshipMovement2D(IRotation2D rotation, IForwardMovement forwardMovement)
        {
            this.rotation = rotation ?? throw new ArgumentNullException(nameof(rotation));
            this.forwardMovement = forwardMovement ?? throw new ArgumentNullException(nameof(forwardMovement));
        }

        public void MoveForward()
        {
            forwardMovement.MoveForward();
        }

        public void Rotate(Direction direction)
        {
            rotation.Rotate(direction);
        }

        public void Stop()
        {
            forwardMovement.Stop();
        }
    }
}

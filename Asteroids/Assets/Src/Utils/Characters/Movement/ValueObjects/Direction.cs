﻿using System;

namespace Utils.Characters.Movement.ValueObjects
{
    public readonly struct Direction
    {
        internal readonly int value;

        private Direction(int value)
        {
            this.value = value;
        }

        public static Direction Left => new Direction(1);

        public static Direction Right => new Direction(-1);

        public static Direction None => new Direction(0);

        public static implicit operator int(Direction d)
        {
            return d.value;
        }

        public static implicit operator Direction(int d)
        {
            return Math.Sign(d);
        }
    }
}

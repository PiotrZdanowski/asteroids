﻿using System;

namespace Utils.Characters.Damage.Interfaces
{
    public interface IDamageable
    {
        void Damage(float damageForce, IDamageSource source);


        event Action<IDamageable, IDamageSource> OnDestroyedBy;
    }
}

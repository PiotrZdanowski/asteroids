﻿using UnityEngine;

namespace GameFlow
{
    class GameUpdater : MonoBehaviour
    {
        private IGameFlowStateMachine<IGameFlowStartState> gameFlow;

        public void Init(IGameFlowStateMachine<IGameFlowStartState> gameFlow)
        {
            this.gameFlow = gameFlow;
        }

        private void Update()
        {
            gameFlow.Update();
        }

        private void FixedUpdate()
        {
            gameFlow.FixedUpdate();
        }
    }
}

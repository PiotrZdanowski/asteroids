﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils.StateMachine;

namespace GameFlow
{
    interface IGameFlowState : IState
    {
        event Action<Type> StateTransitionRequest;
    }
}

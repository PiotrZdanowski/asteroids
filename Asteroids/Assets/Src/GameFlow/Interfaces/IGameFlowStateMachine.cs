﻿using Utils.StateMachine;

namespace GameFlow
{
    interface IGameFlowStateMachine<TStartState>
    {
        IGameFlowState CurrentState { get; }

        void Initialize();

        void SwitchToState<TState>() where TState : IGameFlowState;

        void Update();

        void FixedUpdate();
    }
}

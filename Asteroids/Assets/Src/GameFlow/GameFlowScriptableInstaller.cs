using UnityEngine;
using Zenject;
using GameFlow;
using Utils.StateMachine;
using GameFlow.ConcreteStates.MainMenu;
using GameFlow.ConcreteStates.Managing;
using GameFlow.ConcreteStates.Gameplay;

[CreateAssetMenu(fileName = "GameFlowScriptableInstaller", menuName = "Installers/GameFlowScriptableInstaller")]
public class GameFlowScriptableInstaller : ScriptableObjectInstaller<GameFlowScriptableInstaller>
{
    [SerializeField] private ScenesConfiguration scenesConfiguration = null;
    [SerializeField] private SceneControllerContainer sceneControllerContainer = null;

    public override void InstallBindings()
    {
        Container.Bind<IScenesConfiguration>().FromInstance(scenesConfiguration).AsSingle();
        Container.Bind<IStatesFactory>().To<GameFlowStatesFactory>().FromNew().AsTransient();
        Container.Bind<IStateMachine<IGameFlowStartState>>().To<StateMachine<IGameFlowStartState>>().FromNew().AsSingle();
        Container.Bind<IGameFlowStateMachine<IGameFlowStartState>>().To<GameFlowStateMachine<IGameFlowStartState>>().FromNew().AsSingle().CopyIntoAllSubContainers();
        Container.Bind<ISceneControllerContainer>().FromInstance(sceneControllerContainer).AsSingle();

        Container.Bind<IMainMenuSceneController>().FromMethod(sceneControllerContainer.GetSceneControllerReferenceAs<IMainMenuSceneController>);
        Container.Bind<IGameplaySceneController>().FromMethod(sceneControllerContainer.GetSceneControllerReferenceAs<IGameplaySceneController>);

        Container.Bind<MainMenuState>().FromNew().AsTransient();
        Container.Bind<GameplayState>().FromNew().AsTransient();

        GameFlowTransitionsManager transitionsManager = new GameFlowTransitionsManager(Container.Resolve<IGameFlowStateMachine<IGameFlowStartState>>());
        transitionsManager.Start();
    }
}
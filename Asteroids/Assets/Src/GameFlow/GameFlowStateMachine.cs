﻿using System;
using UnityEngine;
using Utils.StateMachine;

namespace GameFlow
{
    class GameFlowStateMachine<TStartState> : IGameFlowStateMachine<TStartState> where TStartState : IGameFlowStartState
    {
        private IStateMachine<TStartState> stateMachine;

        public IGameFlowState CurrentState => stateMachine.CurrentState as IGameFlowState;

        public GameFlowStateMachine(IStateMachine<TStartState> stateMachine)
        {
            this.stateMachine = stateMachine ?? throw new ArgumentNullException(nameof(stateMachine));
        }

        public void FixedUpdate()
        {
            stateMachine.FixedUpdate();
        }

        public void Initialize()
        {
            stateMachine.Initialize();
        }

        public void SwitchToState<TState>() where TState : IGameFlowState
        {
            stateMachine.SwitchToState<TState>();
        }

        public void Update()
        {
            stateMachine.Update();
        }
    }
}

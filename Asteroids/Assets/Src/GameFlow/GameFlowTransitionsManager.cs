﻿using UnityEngine;
using System;

namespace GameFlow
{
    internal class GameFlowTransitionsManager
    {
        private IGameFlowStateMachine<IGameFlowStartState> gameFlow;

        public GameFlowTransitionsManager(IGameFlowStateMachine<IGameFlowStartState> gameFlow)
        {
            this.gameFlow = gameFlow ?? throw new ArgumentNullException(nameof(gameFlow));
        }

        public void Start()
        {
            GameObject updater = new GameObject("UPDATER");
            GameObject.DontDestroyOnLoad(updater);
            updater.AddComponent<GameUpdater>().Init(gameFlow);
            gameFlow.Initialize();
            gameFlow.CurrentState.StateTransitionRequest += OnCurrentStateTransitionRequest;
        }

        private void OnCurrentStateTransitionRequest(Type nextStateType)
        {
            gameFlow.CurrentState.StateTransitionRequest -= OnCurrentStateTransitionRequest;
            gameFlow.GetType().GetMethod("SwitchToState").MakeGenericMethod(nextStateType).Invoke(gameFlow, Array.Empty<object>());
            gameFlow.CurrentState.StateTransitionRequest += OnCurrentStateTransitionRequest;
        }
    }
}

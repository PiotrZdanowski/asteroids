﻿using GameFlow.ConcreteStates.Managing;
using System;
using UnityEngine;

namespace GameFlow.ConcreteStates.MainMenu
{
    class MainMenuState : IGameFlowState
    {
        private IMainMenuSceneController mainMenu;

        public event Action<Type> StateTransitionRequest = delegate { };

        public MainMenuState(IMainMenuSceneController mainMenu)
        {
            this.mainMenu = mainMenu ?? throw new ArgumentNullException(nameof(mainMenu));
        }

        public void Initialize()
        {
            mainMenu.OnGameStartRequested += OnGameStartRequested;
            Debug.Log("Initialized MainMenu!");
        }

        private void OnGameStartRequested()
        {
            StateTransitionRequest(typeof(SceneLoading.SceneLoadingState<Gameplay.GameplayState>));
        }

        public void UpdateState()
        {
            mainMenu.UpdateState();
        }

        public void FixedUpdateState()
        {
            mainMenu.FixedUpdateState();
        }

        public void Deinitialize()
        {

        }
    }
}

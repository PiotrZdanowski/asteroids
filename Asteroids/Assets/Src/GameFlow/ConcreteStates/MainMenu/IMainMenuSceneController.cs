﻿using GameFlow.ConcreteStates.Managing;
using System;

namespace GameFlow.ConcreteStates.MainMenu
{
    public interface IMainMenuSceneController : ISceneController
    {
        event Action OnGameStartRequested;
    }
}

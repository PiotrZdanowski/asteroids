﻿using GameFlow.ConcreteStates.Managing;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameFlow.ConcreteStates.SceneLoading
{
    class SceneLoadingState<TNextState> : IGameFlowStartState where TNextState : IGameFlowState
    {
        private ISceneControllerContainer sceneControllerContainer;
        private string loadingSceneId;
        private string sceneToLoadId;

        private AsyncOperation loadingSceneOperation;

        public event Action<Type> StateTransitionRequest = delegate { };

        public SceneLoadingState(string loadingSceneId, string sceneToLoadId, ISceneControllerContainer sceneControllerContainer)
        {
            this.loadingSceneId = loadingSceneId ?? throw new ArgumentNullException(nameof(loadingSceneId));
            this.sceneToLoadId = sceneToLoadId ?? throw new ArgumentNullException(nameof(sceneToLoadId));
            this.sceneControllerContainer = sceneControllerContainer ?? throw new ArgumentNullException(nameof(sceneControllerContainer));
        }

        public void Deinitialize()
        {
            Debug.Log(string.Format("Unloading scene: {0}", loadingSceneId));
            SceneManager.UnloadSceneAsync(loadingSceneId);
        }

        public void Initialize()
        {
            Debug.Log(string.Format("Loading scene: {0}", loadingSceneId));
            SceneManager.LoadScene(loadingSceneId, LoadSceneMode.Single);
            Debug.Log(string.Format("Loading scene: {0}", sceneToLoadId));
            loadingSceneOperation = SceneManager.LoadSceneAsync(sceneToLoadId, LoadSceneMode.Additive);
            loadingSceneOperation.allowSceneActivation = false;
        }

        public void UpdateState()
        {
            if(loadingSceneOperation.progress > 0.9f)
            {
                loadingSceneOperation.allowSceneActivation = true;
            }
            if (loadingSceneOperation.isDone && sceneControllerContainer.GetSceneControllerReferenceAs<ISceneController>() != null)
            {
                StateTransitionRequest(typeof(TNextState));
            }
        }

        public void FixedUpdateState()
        {

        }
    }
}

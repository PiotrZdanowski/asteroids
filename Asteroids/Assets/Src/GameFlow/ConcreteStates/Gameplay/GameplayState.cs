﻿using GameFlow.ConcreteStates.Managing;
using System;
using UnityEngine;

namespace GameFlow.ConcreteStates.Gameplay
{
    class GameplayState : IGameFlowState
    {
        private IGameplaySceneController gameplay;

        public event Action<Type> StateTransitionRequest = delegate { };

        public GameplayState(IGameplaySceneController gameplay)
        {
            this.gameplay = gameplay ?? throw new ArgumentNullException(nameof(gameplay));
        }

        public void Initialize()
        {
            gameplay.OnReturnToMenuRequested += OnReturnToMenuRequested;
            gameplay.OnRestartRequested += OnRestartRequested;
            Debug.Log("Initialized Gameplay!");
        }

        private void OnRestartRequested()
        {
            StateTransitionRequest(typeof(SceneLoading.SceneLoadingState<GameplayState>));
        }

        private void OnReturnToMenuRequested()
        {
            StateTransitionRequest(typeof(SceneLoading.SceneLoadingState<MainMenu.MainMenuState>));
        }

        public void UpdateState()
        {
            gameplay.UpdateState();
        }

        public void FixedUpdateState()
        {
            gameplay.FixedUpdateState();
        }

        public void Deinitialize()
        {

        }
    }
}

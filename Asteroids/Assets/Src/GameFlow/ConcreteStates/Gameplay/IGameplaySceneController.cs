﻿using GameFlow.ConcreteStates.Managing;
using System;

namespace GameFlow.ConcreteStates.Gameplay
{
    public interface IGameplaySceneController : ISceneController
    {
        event Action OnReturnToMenuRequested;
        event Action OnRestartRequested;
    }
}

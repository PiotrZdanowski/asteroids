﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace GameFlow.ConcreteStates.Managing
{
    [CreateAssetMenu(fileName = "SceneControllerContainer", menuName = "Asteroids/GameFlow/SceneControllerContainer")]
    public class SceneControllerContainer : ScriptableObject, ISceneControllerContainer
    {
        private ISceneController currentSceneController;

        public T GetSceneControllerReferenceAs<T>() where T : class, ISceneController
        {
            return currentSceneController as T;
        }

        public void SetSceneControllerReference(ISceneController reference)
        {
            currentSceneController = reference;
        }
    }
}

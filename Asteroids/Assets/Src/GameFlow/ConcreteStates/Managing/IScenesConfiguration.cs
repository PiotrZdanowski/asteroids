﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameFlow.ConcreteStates.Managing
{
    interface IScenesConfiguration
    {
        string GetLoadingSceneId();
        string GetMainMenuSceneId();
        string GetGameplaySceneId();
    }
}

﻿using Utils.StateMachine;
using GameFlow.ConcreteStates.SceneLoading;
using GameFlow.ConcreteStates.MainMenu;
using Zenject;
using System;
using GameFlow.ConcreteStates.Gameplay;

namespace GameFlow.ConcreteStates.Managing
{
    internal class GameFlowStatesFactory : IStatesFactory
    {
        private ISceneControllerContainer sceneControllerContainer;
        private IScenesConfiguration scenesConfiguration;
        private DiContainer diContainer;

        public GameFlowStatesFactory(IScenesConfiguration scenesConfiguration, DiContainer diContainer, ISceneControllerContainer sceneControllerContainer)
        {
            this.scenesConfiguration = scenesConfiguration ?? throw new ArgumentNullException(nameof(scenesConfiguration));
            this.diContainer = diContainer ?? throw new ArgumentNullException(nameof(diContainer));
            this.sceneControllerContainer = sceneControllerContainer ?? throw new ArgumentNullException(nameof(sceneControllerContainer));
        }

        public TState CteateState<TState>(IState currentState = null) where TState : IState
        {
            //start state
            if(typeof(TState) == typeof(IGameFlowStartState))
            {
                return (TState)(IState)GetStateThroughLoading<MainMenuState>(scenesConfiguration.GetMainMenuSceneId());
            }
            //main menu
            if (typeof(TState) == typeof(SceneLoadingState<MainMenuState>))
            {
                return (TState)(IState)GetStateThroughLoading<MainMenuState>(scenesConfiguration.GetMainMenuSceneId());
            }
            if (typeof(TState) == typeof(MainMenuState))
            {
                return (TState)(IState)diContainer.Resolve<MainMenuState>();
            }
            //gameplay
            if (typeof(TState) == typeof(SceneLoadingState<GameplayState>))
            {
                return (TState)(IState)GetStateThroughLoading<GameplayState>(scenesConfiguration.GetGameplaySceneId());
            }
            if (typeof(TState) == typeof(GameplayState))
            {
                return (TState)(IState)diContainer.Resolve<GameplayState>();
            }
            throw new System.Exception("State type not recognized!");
        }

        private SceneLoadingState<TStateToLoad> GetStateThroughLoading<TStateToLoad>(string sceneToLoadId) where TStateToLoad : IGameFlowState
        {
            return new SceneLoadingState<TStateToLoad>(scenesConfiguration.GetLoadingSceneId(), sceneToLoadId, sceneControllerContainer);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameFlow.ConcreteStates.Managing
{
    public interface ISceneController
    {
        void UpdateState();
        void FixedUpdateState();
    }
}

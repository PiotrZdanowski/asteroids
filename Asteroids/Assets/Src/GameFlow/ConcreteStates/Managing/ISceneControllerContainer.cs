﻿using UnityEngine;

namespace GameFlow.ConcreteStates.Managing
{
    public interface ISceneControllerContainer
    {
        void SetSceneControllerReference(ISceneController reference);

        T GetSceneControllerReferenceAs<T>() where T : class, ISceneController;
    }
}

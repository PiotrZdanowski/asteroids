﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameFlow.ConcreteStates.Managing
{

    [CreateAssetMenu(fileName = "ScenesConfiguration", menuName = "Asteroids/GameFlow/ScenesConfiguration")]
    class ScenesConfiguration : ScriptableObject, IScenesConfiguration
    {
        [SerializeField] private string loadingSceneName = "";
        [SerializeField] private string mainMenuSceneName = "";
        [SerializeField] private string gameplaySceneName = "";

        public string GetGameplaySceneId()
        {
            return gameplaySceneName;
        }

        public string GetLoadingSceneId()
        {
            return loadingSceneName;
        }

        public string GetMainMenuSceneId()
        {
            return mainMenuSceneName;
        }
    }
}

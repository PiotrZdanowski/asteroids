﻿using System;

namespace GameCore.Gameplay.Interfaces
{
    public interface IPlayer : ITickable
    {
        event Action<IPlayer> OnDestroyed;
    }
}

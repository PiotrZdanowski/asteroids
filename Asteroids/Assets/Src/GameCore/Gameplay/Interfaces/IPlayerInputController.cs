﻿using GameCore.Gameplay.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCore.Gameplay.Interfaces
{
    public interface IPlayerInputController
    {
        PlayerInput CollectPlayerInput();
    }
}

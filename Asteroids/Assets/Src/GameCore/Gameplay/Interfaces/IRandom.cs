﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCore.Gameplay.Interfaces
{
    public interface IRandom
    {
        float Value { get; }

        float Range(float v1, float v2);
        float Range(float v);
        int Range(int v);
    }
}

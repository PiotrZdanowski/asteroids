﻿using UnityEngine;

namespace GameCore.Gameplay.Interfaces
{
    public interface IAsteroidsFactory
    {
        int MaxAsteroidLevel { get; }

        IAsteroid CreateAsteroidAt(Vector3 position, int level);
    }
}

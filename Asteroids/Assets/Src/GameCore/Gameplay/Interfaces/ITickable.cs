﻿namespace GameCore.Gameplay.Interfaces
{
    public interface ITickable
    {
        void Tick();
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Utils.Characters.Damage.Interfaces;

namespace GameCore.Gameplay.Interfaces
{
    public interface IAsteroid : ITickable, IDamageable, IDamageSource
    {
        int ScorePoints { get; }

        void LaunchInDirection(Vector3 direction);

        event Action<IAsteroid, IAsteroid[]> OnFallenApart;
    }
}

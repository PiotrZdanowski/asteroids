﻿using Utils.Characters.Movement.ValueObjects;

namespace GameCore.Gameplay.ValueObjects
{
    public readonly struct PlayerInput
    {
        public readonly bool isMoveForwardPressed;
        public readonly Direction rotationValue;
        public readonly bool isShootPressed;

        public bool IsRotationPressed => rotationValue != Direction.None;

        public PlayerInput(bool isMoveForwardPressed, Direction rotationValue, bool isShootPressed)
        {
            this.isMoveForwardPressed = isMoveForwardPressed;
            this.rotationValue = rotationValue;
            this.isShootPressed = isShootPressed;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCore.Gameplay.ValueObjects
{
    public readonly struct EndButtonsData
    {
        public readonly int score;
        public readonly Action OnMainMenuPressed;
        public readonly Action OnRestartButtonPressed;

        public EndButtonsData(int score, Action onMainMenuPressed, Action onRestartButtonPressed)
        {
            this.score = score;
            OnMainMenuPressed = onMainMenuPressed ?? throw new ArgumentNullException(nameof(onMainMenuPressed));
            OnRestartButtonPressed = onRestartButtonPressed ?? throw new ArgumentNullException(nameof(onRestartButtonPressed));
        }
    }
}

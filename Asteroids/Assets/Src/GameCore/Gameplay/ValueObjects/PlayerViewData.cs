﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCore.Gameplay.ValueObjects
{
    public readonly struct PlayerViewData
    {
        public readonly int hearts;
        public readonly int maxHearts;

        public PlayerViewData(int hearts, int maxHearts)
        {
            this.hearts = hearts;
            this.maxHearts = maxHearts;
        }
    }
}

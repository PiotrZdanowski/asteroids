using GameCore.Gameplay.Concrete;
using GameCore.Gameplay.Concrete.Environment;
using GameCore.Gameplay.Concrete.Inputs;
using GameCore.Gameplay.Concrete.Levels;
using GameCore.Gameplay.Concrete.LevelsController;
using GameCore.Gameplay.Concrete.Player;
using GameCore.Gameplay.Concrete.Player.Weapon;
using GameCore.Gameplay.Concrete.Player.Weapon.Bullets;
using GameCore.Gameplay.Interfaces;
using GameFlow.ConcreteStates.Gameplay;
using GameFlow.ConcreteStates.Managing;
using UnityEngine;
using Utils.Characters.Movement.Interfaces;
using Utils.Characters.UnityAdapters;
using Utils.ObjectPooling.Concrete;
using Utils.ObjectPooling.Interfaces;
using Utils.ScoreSystem.Concrete;
using Utils.ScoreSystem.Interfaces;
using Zenject;

namespace GameCore.Gameplay.Installers
{
    public class GameplayInstaller : MonoInstaller
    {
        [SerializeField] private SceneControllerContainer sceneControllerContainer = null;
        [SerializeField] private GameplaySceneController gameplaySceneController = null;
        [SerializeField] private Player player = null;
        [SerializeField] private PlayerInputController playerInputController = null;
        [SerializeField] private Environment environment = null;
        [SerializeField] new private Camera camera = null;
        [SerializeField] private AsteroidsFactory asteroidsFactory = null;
        [SerializeField] private LevelsController levelsController = null;
        [SerializeField] private BulletsFactory bulletsFactory = null;

        public override void InstallBindings()
        {
            Container.Bind<ISceneControllerContainer>().FromInstance(sceneControllerContainer).AsSingle();
            Container.Bind<IGameplaySceneController>().FromInstance(gameplaySceneController).AsSingle();
            Container.Bind<IPlayerInputController>().FromInstance(playerInputController).AsSingle();
            Container.Bind<IPlayer>().FromInstance(player).AsSingle();
            Container.Bind<IEnvironment>().FromInstance(environment).AsSingle();
            Container.Bind<Camera>().FromInstance(camera).AsSingle();
            Container.Bind<IAsteroidsFactory>().FromInstance(asteroidsFactory).AsSingle();
            Container.Bind<ILevelsController>().FromInstance(levelsController).AsSingle();
            Container.Bind<ITime>().To<TimeAdapter>().FromNew().AsTransient();
            Container.Bind<IObjectFactory<IBullet>>().FromInstance(bulletsFactory).AsSingle();
            Container.Bind<IObjectPool<IBullet>>().To<ObjectPool<IBullet>>().FromNew().AsSingle();
            Container.Bind<IScoreSystem>().To<ScoreSystem>().FromNew().AsSingle();

            sceneControllerContainer.SetSceneControllerReference(gameplaySceneController);
        }
    }
}
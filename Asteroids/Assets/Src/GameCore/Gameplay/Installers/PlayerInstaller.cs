using GameCore.Gameplay.Concrete.Inputs;
using Utils.Characters.UnityAdapters;
using UnityEngine;
using Zenject;
using Utils.Characters.Movement.Interfaces;
using GameCore.Gameplay.Concrete.Player.States;
using Utils.StateMachine;
using GameCore.Gameplay.Concrete.Player.Health;
using GameCore.Gameplay.Concrete.Player.Model;
using GameCore.Gameplay.Concrete.Player.Weapon;

namespace GameCore.Gameplay.Installers
{
    public class PlayerInstaller : MonoInstaller
    {
        [SerializeField] private SpaceshipMovement2DScript movementScript = null;
        [SerializeField] private PlayerStatesFactory playerStatesFactory = null;
        [SerializeField] private PlayerHealth playerHealth = null;
        [SerializeField] private PlayerModel playerModel = null;
        [SerializeField] private SpaceshipGun gun = null;

        public override void InstallBindings()
        {
            Container.Bind<ISpaceshipMovement2D>().FromInstance(movementScript).AsSingle(); //WARNING! TODO: IPlayerMovement with adapter. This causes all objects on scene bind to player movement script.
            Container.Bind<IPlayerStatesFactory>().FromInstance(playerStatesFactory).AsSingle();
            Container.Bind<IStateMachine<AliveState, IPlayerStatesFactory>>().To<StateMachine<AliveState, IPlayerStatesFactory>>().FromNew().AsSingle();
            Container.Bind<IPlayerHealth>().FromInstance(playerHealth).AsSingle();
            Container.Bind<IPlayerModel>().FromInstance(playerModel).AsSingle();
            Container.Bind<IWeapon>().FromInstance(gun).AsSingle();
        }
    }
}
﻿using GameCore.Gameplay.Interfaces;
using UnityEngine;
using Util;
using Zenject;

namespace GameCore.Gameplay.Concrete.Environment
{
    class Environment : MonoBehaviour, IEnvironment
    {
        [Inject] new private Camera camera = null;

        [SerializeField] private EdgeCollider2D playerEdgeCollider = null;
        [SerializeField] private EdgeCollider2D astyeroidsDestroyCollider = null;
        [SerializeField] private float colliderOffset = 5;

        public void GeneratePlayerMovementLimitCollider()
        {
            new ScreenEdgeCollidersGenerator(camera).Generate2DColliders(playerEdgeCollider, camera.farClipPlane, 0);
        }

        public void GenerateAsteroidsDestroyCollider()
        {
            new ScreenEdgeCollidersGenerator(camera).Generate2DColliders(astyeroidsDestroyCollider, camera.farClipPlane, colliderOffset);
        }
    }
}

﻿using GameCore.Gameplay.Interfaces;
using GameCore.Gameplay.ValueObjects;
using Utils.Characters.Movement.ValueObjects;
using UnityEngine;

namespace GameCore.Gameplay.Concrete.Inputs
{
    class PlayerInputController : MonoBehaviour, IPlayerInputController
    {
        public PlayerInput CollectPlayerInput()
        {
            bool isMoveForwardPressed = Input.GetAxis("Vertical") > 0;
            Direction rotationDirection;
            if(Input.GetAxis("Horizontal") > 0)
            {
                rotationDirection = Direction.Right;
            }
            else if(Input.GetAxis("Horizontal") < 0)
            {
                rotationDirection = Direction.Left;
            }
            else
            {
                rotationDirection = Direction.None;
            }
            bool isShootingPressed = Input.GetAxis("Fire1") > 0;

            return new PlayerInput(isMoveForwardPressed, rotationDirection, isShootingPressed);
        }
    }
}

﻿using GameCore.Gameplay.Interfaces;
using System;
using UnityEngine;
using Utils.Characters.Movement.Interfaces;
using Utils.Characters.UnityAdapters;
using Utils.Characters.Damage.Interfaces;
using Utils.CharactersUnityAdapters.Components;
using GameCore.Gameplay.Concrete.Player.Weapon;

namespace GameCore.Gameplay.Concrete.Levels
{
    [RequireComponent(typeof(SpaceshipMovement2DScript), typeof(DamagingCollisionsDetector))]
    class Asteroid : MonoBehaviour, IAsteroid
    {
        [SerializeField] private int piecesToSplitCount = 2;
        [SerializeField] private float invincibilityTime = 1;
        [SerializeField] private float spawnedAsteroidsCenterOffset = 0.1f;
        [SerializeField] private int scorePoints = 1;

        private SpaceshipMovement2DScript movementScript;
        private int level;
        private IAsteroidsFactory asteroidsFactory;
        private ITime time;

        public event Action<IDamageable, IDamageSource> OnDestroyedBy = delegate { };
        public event Action<IAsteroid, IAsteroid[]> OnFallenApart = delegate { };

        private bool IsInvincible => invincibilityTime > 0;

        public int ScorePoints => scorePoints;

        public void Initialize(int level, IAsteroidsFactory asteroidsFactory, ITime time)
        {
            movementScript = gameObject.GetComponent<SpaceshipMovement2DScript>();
            gameObject.GetComponent<DamagingCollisionsDetector>().OnDamageableHit += OnDamageableHit;
            this.level = level;
            this.asteroidsFactory = asteroidsFactory;
            this.time = time;
        }

        private void OnDamageableHit(IDamageable damageable)
        {
            damageable.Damage(float.PositiveInfinity, this);
        }

        public void LaunchInDirection(Vector3 direction)
        {
            float angle = Vector3.SignedAngle(Vector3.up, direction, Vector3.forward);
            transform.Rotate(0, 0, angle);
            movementScript.MoveForward();
        }

        public void Tick()
        {
            invincibilityTime -= time.DeltaTime;
        }

        public void Damage(float damageForce, IDamageSource source)
        {
            if (source is IAsteroid asteroid || source is IBullet)
            {
                if (level >= 0 && !IsInvincible)
                {
                    FallApart(out IAsteroid[] asteroids);
                    OnFallenApart(this, asteroids);
                    OnDestroyedBy(this, source);
                    DestroyBy(source);
                }
            }
            else
            {
                DestroyBy(source);
            }
        }

        private void DestroyBy(IDamageSource source)
        {
            OnDestroyedBy(this, source);
            Destroy(gameObject);
        }

        public void FallApart(out IAsteroid[] splitAsteroids)
        {
            splitAsteroids = new IAsteroid[piecesToSplitCount];
            float angleOffset = piecesToSplitCount > 1 ? 360 / piecesToSplitCount : 0;
            for(int i =0; i < piecesToSplitCount; i++)
            {
                Vector3 offset = Quaternion.Euler(0, 0, angleOffset * i) * Vector3.up * spawnedAsteroidsCenterOffset;
                IAsteroid newAsteroid = asteroidsFactory.CreateAsteroidAt(transform.position + offset, level - 1);
                newAsteroid.LaunchInDirection((transform.up + offset).normalized);
                splitAsteroids[i] = newAsteroid;
            }
        }
    }
}

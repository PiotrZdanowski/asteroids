﻿using GameCore.Gameplay.Concrete.Player.Weapon;
using GameCore.Gameplay.Interfaces;
using System.Collections.Generic;
using UnityEngine;
using Utils.Characters.Damage.Interfaces;
using Utils.Characters.Movement.Interfaces;
using Utils.ScoreSystem.Interfaces;
using Zenject;

namespace GameCore.Gameplay.Concrete.LevelsController
{
    public class LevelsController : UnityEngine.MonoBehaviour, ILevelsController
    {
        [Inject] private IAsteroidsFactory asteroidsFactory = null;
        [Inject] private ITime time = null;
        [Inject] new public Camera camera = null;
        [Inject] private IRandom random = null;
        [Inject] private IScoreSystem score = null;

        [SerializeField] private float asteroidSpawnDistanceBehindEdge = 2f;
        [SerializeField] private float maxRandomLaunchAngle = 60f;
        [SerializeField] private float timeBetweenAsteroids = 1f;
        [SerializeField] private float levelTime = 30f;
        [SerializeField] private float afterAllAsteroidsDestroyedRestTime = 3f;
        [SerializeField] private int scoreOnLevelCompleted = 5;

        private int currentLevel = 0;
        private float currentLevelTimeLeft = 0;
        private float timeToNextAsteroid = 0;
        private int spawnedAsteroidsInThisLevel = 0;
        private HashSet<IAsteroid> activeAsteroids = new HashSet<IAsteroid>();

        public void Tick()
        {
            foreach(IAsteroid asteroid in activeAsteroids)
            {
                asteroid.Tick();
            }
            currentLevelTimeLeft -= time.DeltaTime;
            if(currentLevelTimeLeft <= 0)
            {
                if(currentLevel > 0)
                {
                    score.AddScore(scoreOnLevelCompleted);
                }
                currentLevel++;
                Debug.Log(string.Format("Next level: {0}", currentLevel));
                currentLevelTimeLeft = levelTime;
                timeToNextAsteroid = timeBetweenAsteroids;
                spawnedAsteroidsInThisLevel = 0;
            }
            if(timeToNextAsteroid <= 0 && spawnedAsteroidsInThisLevel < currentLevel)
            {
                SpawnNextAsteroid();
                spawnedAsteroidsInThisLevel++;
                timeToNextAsteroid = timeBetweenAsteroids;
            }
            timeToNextAsteroid -= time.DeltaTime;
        }

        private void SpawnNextAsteroid()
        {
            Vector3 spawnPosition = RandomizePositionOnScreenEdge();
            spawnPosition.z = 0;
            spawnPosition += spawnPosition.normalized * asteroidSpawnDistanceBehindEdge;
            IAsteroid asteroid = asteroidsFactory.CreateAsteroidAt(spawnPosition, random.Range(asteroidsFactory.MaxAsteroidLevel));
            asteroid.LaunchInDirection(GetRandomDirectionFrom(spawnPosition));
            BindAsteroid(asteroid);
        }

        private void BindAsteroid(IAsteroid asteroid)
        {
            asteroid.OnFallenApart += OnAsteroidFallenApart;
            asteroid.OnDestroyedBy += OnAsteroidDestroyedBy;
            activeAsteroids.Add(asteroid);
        }

        private void OnAsteroidFallenApart(IAsteroid asteroid, IAsteroid[] asteroidPieces)
        {
            foreach(IAsteroid a in asteroidPieces)
            {
                BindAsteroid(a);
            }
        }

        private void OnAsteroidDestroyedBy(IDamageable asteroid, IDamageSource source)
        {
            asteroid.OnDestroyedBy -= OnAsteroidDestroyedBy;
            activeAsteroids.Remove(asteroid as IAsteroid);
            if (activeAsteroids.Count == 0)
            {
                currentLevelTimeLeft = Mathf.Min(afterAllAsteroidsDestroyedRestTime, currentLevelTimeLeft);
            }
            if(source is IBullet)
            {
                score.AddScore((asteroid as IAsteroid).ScorePoints);
            }
        }

        private Vector3 GetRandomDirectionFrom(Vector3 randomPositionOnScreenEdge)
        {
            Vector3 toCenter = (Vector3.zero - randomPositionOnScreenEdge).normalized;
            float randomAngle = random.Range(-maxRandomLaunchAngle, maxRandomLaunchAngle);
            return Quaternion.Euler(0, 0, randomAngle) * toCenter;
        }

        private Vector3 RandomizePositionOnScreenEdge()
        {
            Vector3[] corners = new Vector3[4];
            camera.CalculateFrustumCorners(new Rect(0, 0, 1, 1), camera.farClipPlane, Camera.MonoOrStereoscopicEye.Mono, corners);
            float horizontalWeight = (corners[0] - corners[1]).magnitude;
            float verticalWeight = (corners[1] - corners[2]).magnitude;
            float randomEdgeValue = random.Range(horizontalWeight * 2 + verticalWeight * 2);
            int edgeOffset;
            if(randomEdgeValue < horizontalWeight)
            {
                edgeOffset = 0;
            }
            else if (randomEdgeValue < horizontalWeight * 2)
            {
                edgeOffset = 1;
            }
            else if (randomEdgeValue < (horizontalWeight * 2) + verticalWeight)
            {
                edgeOffset = 2;
            }
            else
            {
                edgeOffset = 3;
            }

            Vector3 chosenCorner1, chosenCorner2;
            if(edgeOffset < corners.Length - 1)
            {
                chosenCorner1 = corners[edgeOffset];
                chosenCorner2 = corners[edgeOffset + 1];
            }
            else
            {
                chosenCorner1 = corners[corners.Length - 1];
                chosenCorner2 = corners[0];
            }
            return Vector3.Lerp(chosenCorner1, chosenCorner2, random.Value);
        }
    }
}

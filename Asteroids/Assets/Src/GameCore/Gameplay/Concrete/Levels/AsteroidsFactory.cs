﻿using GameCore.Gameplay.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Utils.Characters.Movement.Interfaces;
using Zenject;

namespace GameCore.Gameplay.Concrete.Levels
{
    class AsteroidsFactory : MonoBehaviour, IAsteroidsFactory
    {
        [SerializeField] private Asteroid[] asteroidPrefabByLevel = null;
        [SerializeField] private Transform asteroidsParent = null;

        [Inject] private ITime time = null;

        public int MaxAsteroidLevel => asteroidPrefabByLevel.Length;

        public IAsteroid CreateAsteroidAt(Vector3 position, int level)
        {
            Asteroid asteroid = GameObject.Instantiate(asteroidPrefabByLevel[level], position, Quaternion.identity, asteroidsParent);
            asteroid.Initialize(level, this, time);
            return asteroid;
        }
    }
}

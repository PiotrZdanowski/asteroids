﻿using GameCore.Gameplay.Concrete.Player.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils.Characters.Damage.Interfaces;
using Utils.Characters.Movement.Interfaces;
using Utils.StateMachine;

namespace GameCore.Gameplay.Concrete.Player.States
{
    public class InvincibleState<TNextState> : BaseState, IPlayerState where TNextState : IPlayerState
    {
        private float invincibilityTime;
        private ITime time;
        private IPlayerModel model;
        private TNextState currentState;

        public event Action<IPlayerState, Type> OnStateChangeRequest = delegate { };
        public event Action<IDamageable, IDamageSource> OnDestroyedBy;
        public event Action<IPlayerState> OnPlayerDestroyed;

        public InvincibleState(float invincibilityTime, ITime time, IPlayerModel model, TNextState currentState)
        {
            this.invincibilityTime = invincibilityTime;
            this.time = time ?? throw new ArgumentNullException(nameof(time));
            this.model = model ?? throw new ArgumentNullException(nameof(model));
            this.currentState = currentState;
        }

        public override void Initialize()
        {
            model.ShowInvincible();
            currentState?.Initialize();
        }

        public void Damage(float damageForce, IDamageSource source)
        {

        }

        public override void UpdateState()
        {
            currentState?.UpdateState();
            if(invincibilityTime <= 0)
            {
                OnStateChangeRequest(this, typeof(TNextState));
            }
            else
            {
                invincibilityTime -= time.DeltaTime;
            }
        }

        public override void FixedUpdateState()
        {
            currentState?.FixedUpdateState();
        }

        public override void Deinitialize()
        {
            model.HideInvincible();
            currentState?.Deinitialize();
        }
    }
}

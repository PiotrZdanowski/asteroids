﻿using GameCore.Gameplay.Concrete.Player.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils.Characters.Damage.Interfaces;
using Utils.Characters.Movement.Interfaces;
using Utils.StateMachine;

namespace GameCore.Gameplay.Concrete.Player.States
{
    class DeadState : BaseState, IPlayerState
    {
        private IPlayerModel model;
        private ITime time;

        public event Action<IPlayerState, Type> OnStateChangeRequest = delegate { };
        public event Action<IDamageable, IDamageSource> OnDestroyedBy;
        public event Action<IPlayerState> OnPlayerDestroyed;

        public DeadState(ITime time, IPlayerModel model)
        {
            this.time = time ?? throw new ArgumentNullException(nameof(time));
            this.model = model ?? throw new ArgumentNullException(nameof(model));
        }

        public override void Initialize()
        {
            model.ShowDead();
        }

        public void Damage(float damageForce, IDamageSource source)
        {

        }
    }
}

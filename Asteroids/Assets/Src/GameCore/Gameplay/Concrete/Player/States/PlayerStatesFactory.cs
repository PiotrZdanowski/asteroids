﻿using GameCore.Gameplay.Concrete.Player.Health;
using GameCore.Gameplay.Concrete.Player.Model;
using GameCore.Gameplay.Concrete.Player.Weapon;
using GameCore.Gameplay.Interfaces;
using System;
using UnityEngine;
using Utils.Characters.Movement.Interfaces;
using Utils.StateMachine;
using Zenject;

namespace GameCore.Gameplay.Concrete.Player.States
{
    class PlayerStatesFactory : MonoBehaviour, IPlayerStatesFactory
    {
        [SerializeField] private float invincibilityTimeAfterDamage = 1f;

        [Inject] private ITime time = null;
        [Inject] private IPlayerInputController input = null;
        [Inject] private ISpaceshipMovement2D playerMovement = null;
        [Inject] private IPlayerHealth health = null;
        [Inject] private IPlayerModel model = null;
        [Inject] private IWeapon playerWeapon = null;

        public event Action<IPlayerState> OnPlayerStateCreated;

        public TState CteateState<TState>(IState currentState = null) where TState : IState
        {
            if(TypesAreEqual<TState, AliveState>())
            {
                AliveState aliveState = new AliveState(input, playerMovement, health, playerWeapon);
                OnPlayerStateCreated(aliveState);
                return (TState)(IState)aliveState;
            }
            if(TypesAreEqual<TState, DeadState>())
            {
                DeadState deadState = new DeadState(time, model);
                OnPlayerStateCreated(deadState);
                return (TState)(IState)deadState;
            }
            if(TypesAreEqual<TState, InvincibleState<AliveState>>())
            {
                IPlayerState state = new InvincibleState<AliveState>(invincibilityTimeAfterDamage, time, model, new AliveState(input, playerMovement, health, playerWeapon));
                OnPlayerStateCreated(state);
                return (TState)state;
            }
            throw new Exception(string.Format("Type not known: {0}!", typeof(TState)));
        }

        private static bool TypesAreEqual<T1, T2>()
        {
            return typeof(T1) == typeof(T2);
        }

    }
}

﻿using System;
using Utils.StateMachine;

namespace GameCore.Gameplay.Concrete.Player.States
{
    interface IPlayerStatesFactory : IStatesFactory
    {
        event Action<IPlayerState> OnPlayerStateCreated;
    }
}

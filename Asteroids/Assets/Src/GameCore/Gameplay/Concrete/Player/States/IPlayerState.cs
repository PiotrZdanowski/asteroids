﻿using System;
using Utils.Characters.Damage.Interfaces;
using Utils.StateMachine;

namespace GameCore.Gameplay.Concrete.Player.States
{
    public interface IPlayerState : IState, IDamageable
    {
        event Action<IPlayerState, Type> OnStateChangeRequest;
        event Action<IPlayerState> OnPlayerDestroyed;
    }
}

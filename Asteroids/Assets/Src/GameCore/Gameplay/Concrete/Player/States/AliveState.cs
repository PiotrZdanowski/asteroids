﻿using GameCore.Gameplay.Concrete.Player.Health;
using GameCore.Gameplay.Concrete.Player.Weapon;
using GameCore.Gameplay.Interfaces;
using GameCore.Gameplay.ValueObjects;
using System;
using Utils.Characters.Damage.Interfaces;
using Utils.Characters.Movement.Interfaces;
using Utils.StateMachine;

namespace GameCore.Gameplay.Concrete.Player.States
{
    class AliveState : BaseState, IPlayerState
    {
        private IPlayerInputController input = null;
        private ISpaceshipMovement2D movement = null;
        private IPlayerHealth health;
        private IWeapon weapon;

        public event Action<IPlayerState, Type> OnStateChangeRequest = delegate { };
        public event Action<IDamageable, IDamageSource> OnDestroyedBy;
        public event Action<IPlayerState> OnPlayerDestroyed;

        public AliveState(IPlayerInputController input, ISpaceshipMovement2D movement, IPlayerHealth playerHealth, IWeapon weapon)
        {
            this.input = input ?? throw new ArgumentNullException(nameof(input));
            this.movement = movement ?? throw new ArgumentNullException(nameof(movement));
            this.health = playerHealth ?? throw new ArgumentNullException(nameof(playerHealth));
            this.weapon = weapon ?? throw new ArgumentNullException(nameof(weapon));
        }

        public override void Initialize()
        {
            health.OnAllHeartsLost += OnAllHeartsLost;
        }

        private void OnAllHeartsLost(IPlayerHealth obj)
        {
            OnPlayerDestroyed(this);
            OnStateChangeRequest(this, typeof(DeadState));
        }

        public void Damage(float damageForce, IDamageSource source)
        {
            health.Damage();
            if (health.Hearts > 0)
            {
                OnStateChangeRequest(this, typeof(InvincibleState<AliveState>));
            }
        }

        public override void UpdateState()
        {
            PlayerInput inputValues = input.CollectPlayerInput();
            weapon.Tick();
            if (inputValues.isMoveForwardPressed)
            {
                movement.MoveForward();
            }
            if (inputValues.IsRotationPressed)
            {
                movement.Rotate(inputValues.rotationValue);
            }
            if (inputValues.isShootPressed)
            {
                weapon.Use();
            }
        }

        public override void Deinitialize()
        {
            health.OnAllHeartsLost -= OnAllHeartsLost;
        }
    }
}

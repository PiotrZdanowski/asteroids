﻿using GameCore.Gameplay.Concrete.Player.States;
using GameCore.Gameplay.Interfaces;
using GameCore.Gameplay.ValueObjects;
using System;
using UnityEngine;
using Utils.Characters.Damage.Interfaces;
using Utils.Characters.Movement.Interfaces;
using Utils.StateMachine;
using Zenject;

namespace GameCore.Gameplay.Concrete.Player
{
    internal class Player : MonoBehaviour, IPlayer, IDamageable
    {
        [Inject] private IStateMachine<AliveState, IPlayerStatesFactory> playerStates = null;
        [Inject] private IPlayerStatesFactory statesFactory = null;

        public event Action<IPlayer> OnDestroyed = delegate { };
        public event Action<IDamageable, IDamageSource> OnDestroyedBy;

        private void Awake()
        {
            statesFactory.OnPlayerStateCreated += OnNewStateCreated;
            playerStates.Initialize();
        }

        private void OnNewStateCreated(IPlayerState state)
        {
            state.OnStateChangeRequest += OnStateChangeRequest;
            state.OnPlayerDestroyed += OnPlayerDestroyed;
        }

        private void OnPlayerDestroyed(IPlayerState state)
        {
            OnDestroyed(this);
        }

        private void OnStateChangeRequest(IPlayerState currentState, Type nextStateType)
        {
            playerStates.GetType().GetMethod("SwitchToState").MakeGenericMethod(nextStateType).Invoke(playerStates, Array.Empty<object>());
        }

        public void Damage(float damageForce, IDamageSource source)
        {
            (playerStates.CurrentState as IPlayerState).Damage(damageForce, source);
        }

        public void Destroy()
        {
            OnDestroyed(this);
        }

        public void Tick()
        {
            playerStates.Update();
        }
    }
}

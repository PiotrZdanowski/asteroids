﻿using System;
using UnityEngine;
using Utils.Characters.Damage.Interfaces;
using Utils.Characters.UnityAdapters;
using Utils.CharactersUnityAdapters.Components;
using Utils.ObjectPooling.Interfaces;

namespace GameCore.Gameplay.Concrete.Player.Weapon.Bullets
{
    [RequireComponent(typeof(DamagingTriggerDetector))]
    public class Bullet : MonoBehaviour, IBullet, IDamageable
    {
        [SerializeField] private Vector3 resetPosition = Vector3.zero;
        [SerializeField] private SpaceshipMovement2DScript movement = null;
        [SerializeField] private DamagingTriggerDetector collisionsDetector = null;

        public event Action<IBullet> OnDestroyed = delegate { };
        public event Action<IDamageable, IDamageSource> OnDestroyedBy = delegate { };

        private void Awake()
        {
            collisionsDetector.OnDamageableHit += OnDamageableHit;
        }

        public void LaunchInDirection(Vector3 direction)
        {
            float angle = Vector3.SignedAngle(Vector3.up, direction, Vector3.forward);
            transform.Rotate(0, 0, angle);
            movement.MoveForward();
        }

        public void SetPosition(Vector3 position)
        {
            transform.position = position;
        }

        void IObjectPoolObject.Reset()
        {
            transform.position = resetPosition;
            transform.rotation = Quaternion.identity;
            movement.Stop();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
        }

        private void OnDamageableHit(IDamageable damageable)
        {
            damageable.Damage(float.PositiveInfinity, this);
            Damage(float.PositiveInfinity, this);
        }

        public void Damage(float damageForce, IDamageSource source)
        {
            OnDestroyed(this);
            OnDestroyedBy(this, source);
        }
    }
}

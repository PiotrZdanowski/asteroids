﻿using GameCore.Gameplay.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCore.Gameplay.Concrete.Player.Weapon
{
    public interface IWeapon : ITickable
    {
        void Use();
    }
}

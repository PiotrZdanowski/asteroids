﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Utils.Characters.Damage.Interfaces;
using Utils.ObjectPooling.Interfaces;

namespace GameCore.Gameplay.Concrete.Player.Weapon
{
    public interface IBullet : IObjectPoolObject, IDamageSource
    {
        void LaunchInDirection(Vector3 direction);
        void SetPosition(Vector3 position);

        event Action<IBullet> OnDestroyed;
    }
}

﻿using UnityEngine;
using Utils.ObjectPooling.Interfaces;

namespace GameCore.Gameplay.Concrete.Player.Weapon.Bullets
{
    public class BulletsFactory : MonoBehaviour, IObjectFactory<IBullet>
    {
        [SerializeField] private Bullet bulletPrefab = null;
        [SerializeField] private Transform bulletsParent = null;

        public IBullet CreateNewObject()
        {
            return Instantiate(bulletPrefab, bulletsParent);
        }
    }
}

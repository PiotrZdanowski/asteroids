﻿using UnityEngine;
using Utils.Characters.Movement.Interfaces;
using Utils.ObjectPooling.Interfaces;
using Zenject;

namespace GameCore.Gameplay.Concrete.Player.Weapon
{
    class SpaceshipGun : MonoBehaviour, IWeapon, GameCore.Gameplay.Interfaces.ITickable
    {
        [Inject] private IObjectPool<IBullet> bulletsPool = null;
        [Inject] private ITime time = null;

        [SerializeField] private float cooldownTime = 1;
        [SerializeField] private Transform barell = null;

        private float currentCooldownTime;

        private bool IsShotAllowed => currentCooldownTime <= 0;

        public void Tick()
        {
            if(currentCooldownTime > 0)
            {
                currentCooldownTime -= time.DeltaTime;
            }
        }

        public void Use()
        {
            if (IsShotAllowed)
            {
                IBullet bullet = bulletsPool.GetNext();
                bullet.OnDestroyed += OnBulletDestroyed;
                bullet.SetPosition(barell.position);
                bullet.LaunchInDirection(barell.up);
                currentCooldownTime = cooldownTime;
            }
        }

        private void OnBulletDestroyed(IBullet bullet)
        {
            bullet.OnDestroyed -= OnBulletDestroyed;
            bulletsPool.ReturnToPool(bullet);
        }
    }
}

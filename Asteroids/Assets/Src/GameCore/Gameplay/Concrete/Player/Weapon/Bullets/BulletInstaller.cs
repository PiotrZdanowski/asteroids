﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Utils.Characters.Movement.Interfaces;
using Utils.Characters.UnityAdapters;
using Zenject;

namespace GameCore.Gameplay.Concrete.Player.Weapon.Bullets
{
    public class BulletInstaller : MonoInstaller
    {
        [SerializeField] private SpaceshipMovement2DScript movement = null;

        public override void InstallBindings()
        {
            Container.Bind<IForwardMovement>().FromInstance(movement);
        }
    }
}

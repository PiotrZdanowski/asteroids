﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace GameCore.Gameplay.Concrete.Player.Model
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class PlayerModel : MonoBehaviour, IPlayerModel
    {
        [SerializeField] private Color normalColor = Color.white;
        [SerializeField] private Color invincibleColor = Color.green;
        [SerializeField] private Color deadColor = Color.gray;
        private SpriteRenderer spriteRenderer;

        private void Awake()
        {
            spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        }

        public void HideInvincible()
        {
            spriteRenderer.color = normalColor;
        }

        public void ShowDead()
        {
            spriteRenderer.color = deadColor;
        }

        public void ShowInvincible()
        {
            spriteRenderer.color = invincibleColor;
        }
    }
}

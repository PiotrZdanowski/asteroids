﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCore.Gameplay.Concrete.Player.Model
{
    public interface IPlayerModel
    {
        void ShowDead();
        void ShowInvincible();
        void HideInvincible();
    }
}

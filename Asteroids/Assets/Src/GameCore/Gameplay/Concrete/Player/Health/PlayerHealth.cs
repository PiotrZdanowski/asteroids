﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace GameCore.Gameplay.Concrete.Player.Health
{
    public class PlayerHealth : MonoBehaviour, IPlayerHealth
    {
        [SerializeField] private int livesCount = 3;

        private int heartsLost = 0;
        public int Hearts => livesCount - heartsLost;

        public int MaxHearts => livesCount;

        public event Action<IPlayerHealth> OnAllHeartsLost = delegate { };
        public event Action<IPlayerHealth> OnHeartLost = delegate { };

        public void Damage()
        {
            heartsLost++;
            OnHeartLost(this);
            if(Hearts <= 0)
            {
                OnAllHeartsLost.Invoke(this);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCore.Gameplay.Concrete.Player.Health
{
    public interface IPlayerHealth
    {
        int MaxHearts { get; }
        int Hearts { get; }

        void Damage();

        event Action<IPlayerHealth> OnAllHeartsLost;
        event Action<IPlayerHealth> OnHeartLost;
    }
}

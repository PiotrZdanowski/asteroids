﻿using GameCore.Gameplay.Concrete.Player.Health;
using GameCore.Gameplay.Interfaces;
using GameCore.Gameplay.ValueObjects;
using GameFlow.ConcreteStates.Gameplay;
using GameFlow.ConcreteStates.Managing;
using System;
using UnityEngine;
using Utils.ScoreSystem.Interfaces;
using Zenject;

namespace GameCore.Gameplay.Concrete
{
    class GameplaySceneController : MonoBehaviour, IGameplaySceneController
    {
        [Inject] private ISceneControllerContainer controllerContainer = null;
        [Inject] private IPlayer player = null;
        [Inject] private IEnvironment environment = null;
        [Inject] private ILevelsController levelsController = null;
        [Inject] private IScoreSystem scoreSystem = null;
        [Inject] private IPlayerHealth playerHealth = null;
        [Inject] private IGameplayUI gameplayUI = null;//TODO: move UI logic to a UI controller

        public event Action OnReturnToMenuRequested = delegate { };
        public event Action OnRestartRequested = delegate { };

        private bool playerDestroyed; //TODO internal state machine with player alive, player dead

        private void Start()
        {
            controllerContainer.SetSceneControllerReference(this);
            environment.GeneratePlayerMovementLimitCollider();
            environment.GenerateAsteroidsDestroyCollider();
            player.OnDestroyed += OnPlayerDestroyed;
            scoreSystem.OnScoreChanged += OnScoreChanged;
            scoreSystem.OnHighscoreReached += OnHighscoreReached;
            playerHealth.OnHeartLost += OnPlayerHeartLost;

            gameplayUI.UpdatePlayerView(new PlayerViewData(playerHealth.Hearts, playerHealth.MaxHearts));
            gameplayUI.UpdateScoreView(new ScoreViewData(scoreSystem.Highscore, scoreSystem.CurrentScore));
        }

        private void OnPlayerHeartLost(IPlayerHealth playerHealth)
        {
            gameplayUI.UpdatePlayerView(new PlayerViewData(playerHealth.Hearts, playerHealth.MaxHearts));
        }

        private void OnHighscoreReached(IScoreSystem arg1, int arg2)
        {
            gameplayUI.ShowHighscoreBonus();
        }

        private void OnScoreChanged(IScoreSystem arg1, int arg2)
        {
            gameplayUI.UpdateScoreView(new ScoreViewData(scoreSystem.Highscore, scoreSystem.CurrentScore));
        }

        private void OnPlayerDestroyed(IPlayer obj)
        {
            playerDestroyed = true;
            gameplayUI.ShowEndButtons(new EndButtonsData(scoreSystem.CurrentScore, OnReturnToMainMenuClicked, OnRestartClicked));
        }

        private void OnReturnToMainMenuClicked()
        {
            OnReturnToMenuRequested();
        }

        private void OnRestartClicked()
        {
            OnRestartRequested();
        }


        public void FixedUpdateState()
        {

        }

        public void UpdateState()
        {
            player.Tick();
            if (!playerDestroyed)
            {
                levelsController.Tick();
            }
        }
    }
}

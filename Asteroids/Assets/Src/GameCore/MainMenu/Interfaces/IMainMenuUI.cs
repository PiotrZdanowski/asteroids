﻿using System;

namespace GameCore.MainMenu.Interfaces
{
    public interface IMainMenuUI
    {
        void SetStartButtonAction(Action action);

        void ShowHighScore(int score);
    }
}

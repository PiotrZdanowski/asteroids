﻿using UnityEngine;
using Zenject;
using GameCore.MainMenu.Interfaces;
using GameFlow.ConcreteStates.Managing;
using GameFlow.ConcreteStates.MainMenu;
using System;
using Utils.ScoreSystem.Interfaces;

namespace GameCore.MainMenu.Concrete
{
    public class MainMenuState : MonoBehaviour, IMainMenuSceneController
    {
        [Inject] private IMainMenuUI mainMenuUI = null;
        [Inject] private ISceneControllerContainer controllerContainer = null;
        [Inject] private IScoreSystem scoreSystem = null;

        public event Action OnGameStartRequested;

        private void Awake()
        {
            controllerContainer.SetSceneControllerReference(this);
            mainMenuUI.SetStartButtonAction(OnUIStartButton);
            mainMenuUI.ShowHighScore(scoreSystem.Highscore);
        }

        private void OnUIStartButton()
        {
            OnGameStartRequested();
        }

        public void FixedUpdateState()
        {

        }

        public void UpdateState()
        {

        }
    }
}

using GameCore.MainMenu.Concrete;
using GameFlow.ConcreteStates.Managing;
using UnityEngine;
using Utils.ScoreSystem.Concrete;
using Utils.ScoreSystem.Interfaces;
using Zenject;

namespace GameCore.MainMenu.Installers
{
    public class MainMenuInstaller : MonoInstaller
    {
        [SerializeField] private SceneControllerContainer sceneControllerContainer = null;
        [SerializeField] private MainMenuState mainMenuController = null;

        public override void InstallBindings()
        {
            Container.Bind<ISceneControllerContainer>().FromInstance(sceneControllerContainer).AsSingle();
            Container.Bind<IScoreSystem>().To<ScoreSystem>().FromNew().AsSingle();

            sceneControllerContainer.SetSceneControllerReference(mainMenuController);
        }
    }
}
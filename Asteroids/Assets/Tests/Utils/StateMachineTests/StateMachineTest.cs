﻿using NUnit.Framework;
using Utils.StateMachine;
using NSubstitute;

namespace Tests.Utils.StateMachineTests
{
    public class StateMachineTest
    {

        [Test]
        public void ShouldCallCreateStartStateOnInitialize()
        {
            IStatesFactory statesFactorySubstitute = Substitute.For<IStatesFactory>();
            StateMachine<IState> stateMachine = new StateMachine<IState>(statesFactorySubstitute);

            stateMachine.Initialize();

            statesFactorySubstitute.Received(1).CteateState<IState>();
        }

        [Test]
        public void ShouldCallCreateStateOnSwitchingToState()
        {
            IStatesFactory statesFactorySubstitute = Substitute.For<IStatesFactory>();
            StateMachine<IState> stateMachine = new StateMachine<IState>(statesFactorySubstitute);

            stateMachine.Initialize();
            stateMachine.SwitchToState<ITestState>();

            statesFactorySubstitute.Received(1).CteateState<ITestState>(Arg.Any<IState>());
        }

        [Test]
        public void ShouldUpdateStartStateOnUpdateAfterInitialize()
        {
            IState testStartStateSubstitute = Substitute.For<IState>();

            IStatesFactory statesFactorySubstitute = Substitute.For<IStatesFactory>();
            statesFactorySubstitute.CteateState<IState>().Returns(testStartStateSubstitute);

            StateMachine<IState> stateMachine = new StateMachine<IState>(statesFactorySubstitute);

            stateMachine.Initialize();
            stateMachine.Update();
            stateMachine.FixedUpdate();

            testStartStateSubstitute.Received(1).UpdateState();
            testStartStateSubstitute.Received(1).FixedUpdateState();
        }

        [Test]
        public void ShouldUpdateStateOnUpdateAfterInitializeAndSwitchToState()
        {
            ITestState testStateSubstitute = Substitute.For<ITestState>();

            IStatesFactory statesFactorySubstitute = Substitute.For<IStatesFactory>();
            statesFactorySubstitute.CteateState<ITestState>(Arg.Any<IState>()).Returns(testStateSubstitute);

            StateMachine<IState> stateMachine = new StateMachine<IState>(statesFactorySubstitute);

            stateMachine.Initialize();
            stateMachine.SwitchToState<ITestState>();
            stateMachine.Update();
            stateMachine.FixedUpdate();

            testStateSubstitute.Received(1).UpdateState();
            testStateSubstitute.Received(1).FixedUpdateState();
        }

        [Test]
        public void ShouldCallDeinitializeOnOldStateWhenSwitchingToNewState()
        {
            ITestState testStateSubstitute = Substitute.For<ITestState>();

            IStatesFactory statesFactorySubstitute = Substitute.For<IStatesFactory>();
            statesFactorySubstitute.CteateState<ITestState>().Returns(testStateSubstitute);

            StateMachine<ITestState> stateMachine = new StateMachine<ITestState>(statesFactorySubstitute);

            stateMachine.Initialize();
            stateMachine.SwitchToState<IState>();

            testStateSubstitute.Received(1).Deinitialize();
        }

        public interface ITestState : IState
        {

        }
    }
}

﻿using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Utils.Characters.UnityAdapters;
using Utils.Characters.Movement.ValueObjects;

namespace Tests
{
    public class Spaceship2DMovementTest
    {
        private SpaceshipMovement2DScript movementScript;

        [SetUp]
        public void SetUp()
        {
            movementScript = GenerateTestGameObject();
        }

        private SpaceshipMovement2DScript GenerateTestGameObject(float maxSpeed = 1, float force = 1, float rotationSpeed = 60)
        {
            GameObject gameObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
            GameObject.DestroyImmediate(gameObject.GetComponent<Collider>());

            Rigidbody2D rigidbody = gameObject.AddComponent<Rigidbody2D>();
            rigidbody.gravityScale = 0;
            rigidbody.drag = 0;
            rigidbody.angularDrag = 0;

            SpaceshipMovement2DScript script = gameObject.AddComponent<SpaceshipMovement2DScript>();
            script.MaxSpeed = maxSpeed;
            script.MovementForce = force;
            script.RotationSpeed = rotationSpeed;
            return script;
        }

        [TearDown]
        public void TearDown()
        {
            GameObject.DestroyImmediate(movementScript.gameObject);
        }

        [UnityTest]
        public IEnumerator ShouldMoveUpWhenMoveForwardCalled()
        {
            WaitForEndOfFrame waitForEndOfFrame = new WaitForEndOfFrame();
            float previousY = 0;

            float testTime = 1f;
            float afterTestObservationTime = 0f;

            while(testTime > 0)
            {
                movementScript.MoveForward();
                yield return waitForEndOfFrame;

                Assert.IsTrue(movementScript.transform.position.y >= previousY);

                previousY = movementScript.transform.position.y;
                testTime -= Time.deltaTime;
            }

            Assert.IsTrue(movementScript.transform.position.y > 0);

            yield return new WaitForSecondsRealtime(afterTestObservationTime);
        }

        [UnityTest]
        public IEnumerator ShouldIncreaseAngleWhenRotateCalledWithLeftDirection()
        {
            WaitForEndOfFrame waitForEndOfFrame = new WaitForEndOfFrame();
            float previousZRotation = 0;

            float testTime = 1f;
            float afterTestObservationTime = 0f;

            while (testTime > 0)
            {
                movementScript.Rotate(Direction.Left);
                yield return waitForEndOfFrame;

                Assert.IsTrue(movementScript.transform.rotation.eulerAngles.z >= previousZRotation);

                previousZRotation = movementScript.transform.rotation.eulerAngles.z;
                testTime -= Time.deltaTime;
            }
            yield return new WaitForSecondsRealtime(afterTestObservationTime);
        }

        [UnityTest]
        public IEnumerator ShouldNotRotateWhenNoneDirectionApplied()
        {
            WaitForEndOfFrame waitForEndOfFrame = new WaitForEndOfFrame();
            float previousZRotation = 0;

            float testTime = 1f;
            float afterTestObservationTime = 0f;

            while (testTime > 0)
            {
                movementScript.Rotate(Direction.None);
                yield return waitForEndOfFrame;

                Assert.IsTrue(movementScript.transform.rotation.eulerAngles.z == previousZRotation);

                previousZRotation = movementScript.transform.rotation.eulerAngles.z;
                testTime -= Time.deltaTime;
            }
            yield return new WaitForSecondsRealtime(afterTestObservationTime);
        }

        [UnityTest]
        public IEnumerator ShouldDecreaseAngleWhenRotateCalledWithRightDirection()
        {
            WaitForEndOfFrame waitForEndOfFrame = new WaitForEndOfFrame();
            float previousZRotation = 360;

            float testTime = 1f;
            float afterTestObservationTime = 0f;

            while (testTime > 0)
            {
                movementScript.Rotate(Direction.Right);
                yield return waitForEndOfFrame;

                if(movementScript.transform.rotation.eulerAngles.z != 0)
                {
                    Assert.IsTrue(movementScript.transform.rotation.eulerAngles.z <= previousZRotation);
                    previousZRotation = movementScript.transform.rotation.eulerAngles.z;
                }

                testTime -= Time.deltaTime;
            }
            yield return new WaitForSecondsRealtime(afterTestObservationTime);
        }
        
        [UnityTest]
        public IEnumerator ShouldMoveForvardInOtherDirectionWhenRotatedAndMoveForvardCalled()
        {
            WaitForEndOfFrame waitForEndOfFrame = new WaitForEndOfFrame();
            TearDown();
            movementScript = GenerateTestGameObject(1, 1, 90);
            float previousX = 0;

            float rotateTime = 1f;
            float moveForwardTime = 1f;
            float afterTestObservationTime = 0f;

            while (rotateTime > 0)
            {
                movementScript.Rotate(Direction.Left);
                yield return waitForEndOfFrame;
                rotateTime -= Time.deltaTime;
            }
            while(moveForwardTime > 0)
            {
                movementScript.MoveForward();
                yield return waitForEndOfFrame;

                Assert.IsTrue(movementScript.transform.position.x <= previousX);

                previousX = movementScript.transform.position.x;
                moveForwardTime -= Time.deltaTime;
            }
            Assert.IsTrue(movementScript.transform.position.x < 0);
            yield return new WaitForSecondsRealtime(afterTestObservationTime);
        }

        [UnityTest]
        public IEnumerator ShouldTurnWhenRotatingAndMovingAtTheSameTime()
        {
            WaitForEndOfFrame waitForEndOfFrame = new WaitForEndOfFrame();

            float testTime = 2f;
            float afterTestObservationTime = 0f;

            while (testTime > 0)
            {
                movementScript.Rotate(Direction.Right);
                movementScript.MoveForward();
                yield return waitForEndOfFrame;
                testTime -= Time.deltaTime;
            }
            Assert.IsTrue(movementScript.transform.position.magnitude > 0);
            Assert.IsTrue(movementScript.transform.rotation.z != 0);
            yield return new WaitForSecondsRealtime(afterTestObservationTime);
        }

        [UnityTest]
        public IEnumerator ShouldLowerYVelocityWhenTurnedAroundAndMoveForwardCalled()
        {
            WaitForEndOfFrame waitForEndOfFrame = new WaitForEndOfFrame();
            TearDown();
            movementScript = GenerateTestGameObject(1, 1, 180);
            Rigidbody2D rigidbody = movementScript.GetComponent<Rigidbody2D>();

            float rotateTime = 1f;
            float moveForwardTime = 1f;
            float afterTestObservationTime = 0f;

            while (moveForwardTime > 0)
            {
                movementScript.MoveForward();
                yield return waitForEndOfFrame;
                moveForwardTime -= Time.deltaTime;
            }
            while (rotateTime > 0)
            {
                movementScript.Rotate(Direction.Left);
                yield return waitForEndOfFrame;
                rotateTime -= Time.deltaTime;
            }
            moveForwardTime = 1f;
            float startRigidbodyVelocityY = rigidbody.velocity.y;
            float previousRigidbodyVelocityY = startRigidbodyVelocityY;
            while (moveForwardTime > 0)
            {
                movementScript.MoveForward();
                yield return waitForEndOfFrame;

                Assert.IsTrue(rigidbody.velocity.y <= previousRigidbodyVelocityY);

                previousRigidbodyVelocityY = rigidbody.velocity.y;
                moveForwardTime -= Time.deltaTime;
            }
            Assert.IsTrue(rigidbody.velocity.y < startRigidbodyVelocityY);
            yield return new WaitForSecondsRealtime(afterTestObservationTime);
        }
    }
}

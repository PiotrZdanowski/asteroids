﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using TSystems;

namespace Tests.TSystems
{
    public class TSystemTests
    {
        // A Test behaves as an ordinary method
        [Test]
        public void TSystemSimplePasses()
        {
            TSystem tSystem = new TSystem();
            tSystem.DoThing();
            TSystemClass tSystemClass = new TSystemClass();
            tSystemClass.DoThing();
            // Use the Assert class to test conditions
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator TSystemWithEnumeratorPasses()
        {
            // Use the Assert class to test conditions.
            // Use yield to skip a frame.
            yield return null;
        }
    }
}
